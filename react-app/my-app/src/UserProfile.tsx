import { useTranslation } from "react-i18next";
import CustomNavbar from "./CustomNavbar";
import styled from 'styled-components';
import { useEffect, useState } from "react";
import { Button, Checkbox, FormControlLabel, FormGroup, Snackbar } from "@mui/material";
import axios from "axios";
import Alert, { AlertColor } from "@mui/material/Alert";

const CustomMainContainer = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    color: white;

    .loginLink {
        width: 100%;
        height:3%;
        font-size: 20px;
        display: flex;
        justify-content: center;
    }
`;

interface UserPreference {
    id: number;
    description: string;
}

const Input = styled('input')({
    display: 'none',
});

const UserProfile = () => {
    const [firstName, setFName] = useState('');
    const [email, setEmail] = useState('');
    const [severity, setSeverity] = useState<AlertColor>('success');
    const [open, setOpen] = useState(false);
    const [message, setMessage] = useState('');
    const { t } = useTranslation();
    const currentUserEmail: any = localStorage.getItem('email');
    const [studiosPreference, setStudiosPreference] = useState(false);
    const [apartamentsPreference, setApartmentsPreference] = useState(false);
    const [housesPreference, setHousesPreference] = useState(false);
    const [userId, setUserId] = useState(0);
    const [pdfFile, setPdfFile] = useState('');

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    useEffect(() => {
        getUserInfo(currentUserEmail);
    }, [currentUserEmail]);

    const editUser = () => {
        if (email !== "" && firstName !== "") {
            axios.put(`http://127.0.0.1:8000/api/users/${userId}`, { email, firstName })
                .then(({ data }) => {
                    if (data === "ERROR") {
                        setSeverity("error");
                        setOpen(true);
                        setMessage('Failed edit! This email is already taken.');
                    }
                    else {
                        localStorage.setItem('email', email);
                        setSeverity("success");
                        setOpen(true);
                        setMessage('Successful edit of user information!');
                    }
                })
        }
        else {
            setSeverity("error");
            setOpen(true);
            setMessage('Failed edit!');
        }
    }

    const getUserInfo = (email: any) => {
        axios.post('http://127.0.0.1:8000/api/profile', { email })
            .then(({ data }) => {
                setUserId(data[0].id);
                axios.get('http://127.0.0.1:8000/api/userPreferences/' + data[0].id)
                    .then(({ data }) => {

                        data.forEach((x: any) => {
                            const preference: UserPreference = {
                                id: x.id,
                                description: x.description
                            }
                            if (preference.description === 'Studios') {
                                setStudiosPreference(true);
                            }
                            else if (preference.description === 'Apartments') {
                                setApartmentsPreference(true);
                            }
                            else if (preference.description === 'Houses') {
                                setHousesPreference(true);
                            }
                        })
                    });

                setFName(data[0].firstName);
                setEmail(data[0].email);
            })
    }

    const editUserInfo = (user_id: any, preference_id: any) => {
        axios.post('http://127.0.0.1:8000/api/userPreferences/', { user_id, preference_id });
        setSeverity("success");
        setOpen(true);
        setMessage('Successful edit of user information!');
    }

    const deleteUserInfo = (user_id: any, preference_id: any) => {
        axios.delete('http://127.0.0.1:8000/api/userPreferences/delete', { data: { user_id: user_id, preference_id: preference_id } });
        setSeverity("success");
        setOpen(true);
        setMessage('Successful edit of user information!');
    }

    const onChange = (checkedValues: any, value: any) => {
        if (value === 1) {
            setStudiosPreference(!studiosPreference);
            if (checkedValues === true) {
                //delete
                deleteUserInfo(userId, 1);
            }
            else {
                editUserInfo(userId, 1);
            }
        }
        if (value === 2) {
            setApartmentsPreference(!apartamentsPreference);
            if (checkedValues === true) {
                //delete
                deleteUserInfo(userId, 2);
            }
            else {
                editUserInfo(userId, 2);
            }
        }
        if (value === 3) {
            setHousesPreference(!housesPreference);
            if (checkedValues === true) {
                //delete
                deleteUserInfo(userId, 3);
            }
            else {
                editUserInfo(userId, 3);
            }
        }

    }


    return (
        <CustomMainContainer>
            <CustomNavbar />

            <div className="contactContent">
                <form className="contactForm">
                    <label htmlFor="firstName">{t('FIRST_NAME')}</label>
                    <input onChange={(e) => setFName(() => e.target.value)} type="text" id="firstName" name="firstName" value={firstName} /><br />

                    <label htmlFor="email">{t('EMAIL')}</label>
                    <input type="text" onChange={(e) => setEmail(() => e.target.value)} id="email" name="email" value={email} /><br /><br />

                    <FormGroup className="formGroup">
                        <FormControlLabel control={<Checkbox onChange={() => onChange(studiosPreference, 1)} value={1} checked={studiosPreference} />} label={t('STUDIOS')} />
                        <FormControlLabel control={<Checkbox onChange={() => onChange(apartamentsPreference, 2)} value={2} checked={apartamentsPreference} />} label={t('APARTMENTS')} />
                        <FormControlLabel control={<Checkbox onChange={() => onChange(housesPreference, 3)} value={3} checked={housesPreference} />} label={t('HOUSES')} />
                    </FormGroup>

                    <label htmlFor="pdfFile">
                        <input onChange={(e) => { setPdfFile(() => e.target.value); console.log("PDF file: " + e.target.value)}} accept=".pdf" id="pdfFile" multiple type="file" />
                    </label>

                    <div className="buttonContactForm">
                        <button onClick={(e) => { e.preventDefault(); editUser() }} className="contactButton" id="contactButton">{t('SUBMIT')}</button>
                    </div>

                </form>
            </div>
            <div className="snackbar">
                <Snackbar open={open} onClose={handleClose} autoHideDuration={1000} >
                    <Alert severity={severity} sx={{ width: '100%' }}>
                        {message}
                    </Alert>
                </Snackbar>
            </div>
        </CustomMainContainer>
    );
}

export default UserProfile;