import { Button } from "antd";
import { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { Link, useNavigate } from "react-router-dom";
import styled from 'styled-components';
import LogoutIcon from '@mui/icons-material/Logout';
import PersonIcon from '@mui/icons-material/Person';
import { UserContext } from "./shared/routes/UserContext";

const CustomButton = styled(Button)`
    width: 100%;
    height: 100%;
    background: rgb(46, 58, 47);
    color: white;
    border: none;
    font-size: 15px;
`;

const CustomNavbar = () => {
    const { t, i18n } = useTranslation()
    const [language, setLanguage] = useState(false);
    const currentLanguage: any = localStorage.getItem('language');
    const [visible, setVisible] = useState(false);
    const navigate = useNavigate();
    const isAdmin = localStorage.getItem('role') === 'AI' ? true : false;
    const { setRole } = useContext(UserContext);

    useEffect(() => {
        const currentUser: any = localStorage.getItem('firstName');

        i18n.changeLanguage(currentLanguage);
        if (currentUser !== "" && currentUser !== null) {
            setVisible(true);
        }
        else {
            setVisible(false);
            // navigate('/login');
        }
    }, [i18n, currentLanguage]);

    //EN -> True , RO -> False
    const changeLanguage = (language: any) => {
        if (language === false) {
            localStorage.setItem('language', "ro");
            i18n.changeLanguage("ro");
        }
        else {
            localStorage.setItem('language', "en");
            i18n.changeLanguage("en");
        }

        setLanguage(language);
    }

    const logout = () => {
        localStorage.setItem('firstName', "");
        localStorage.setItem('email', "");
        localStorage.setItem('role', "");
        localStorage.setItem('userId', "");
        setRole("");

        navigate('/');
    }

    return (
        <div className="navbar">
            {visible && !isAdmin && <div className="dropdown">
                <Link to="/home"><button className="dropbtn">{t('HOMEPAGE')}</button></Link>
            </div>}

            {visible && isAdmin &&  <div className="dropdown">
                <Link to="/adauga"><button className="dropbtn">{t('ADD')}</button></Link>
            </div>}

            <div className="dropdown">
                <Link to="/noutati"><button className="dropbtn">{t('NEWS')}</button></Link>
            </div>

            <div className="dropdown">
                <Link to="/despre"><button className="dropbtn">{t('ABOUT_US')}</button></Link>
            </div>

            <div className="dropdown">
                <button className="dropbtn">{t('RENT')}</button>
                <div className="dropdown-content">
                    <a href="inchirieri#garsoniereSection">{t('STUDIOS')}</a>
                    <a href="inchirieri#apartamentSection">{t('APARTMENTS')}</a>
                    <a href="inchirieri#homeSection">{t('HOUSES')}</a>
                </div>
            </div>

            <div className="dropdown">
                <button className="dropbtn">{t('SALES')}</button>
                <div className="dropdown-content">
                    <a href="vanzari#garsoniereSection">{t('STUDIOS')}</a>
                    <a href="vanzari#apartamentSection">{t('APARTMENTS')}</a>
                    <a href="vanzari#homeSection">{t('HOUSES')}</a>
                </div>
            </div>

            <div className="dropdown">
                <Link to="/contact"><button className="dropbtn">{t('CONTACT')}</button></Link>
            </div>

            <div className="dropdown">
                <div className="languageSelector">
                    <CustomButton className="switch" onClick={() => changeLanguage(!language)}>{t('LANGUAGE')}</CustomButton>
                </div>
            </div>

            {visible && !isAdmin && <div className="dropdown">
                <Link to="/profile"><button className="dropbtn"><PersonIcon/></button></Link>
            </div>}

            {visible && <div className="dropdown">
                <div className="languageSelector">
                    <CustomButton type="dashed" className="switch" onClick={logout}><LogoutIcon/></CustomButton>
                </div>
            </div>}


        </div>
    );
}

export default CustomNavbar;