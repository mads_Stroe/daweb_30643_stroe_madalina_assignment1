import axios from "axios";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from 'styled-components';
import Snackbar from '@mui/material/Snackbar';
import Alert, { AlertColor } from "@mui/material/Alert";
import { Link, useNavigate } from "react-router-dom";

const CustomMainContainer = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    color: white;

    .loginLink {
        width: 100%;
        height:3%;
        font-size: 20px;
        display: flex;
        justify-content: center;
    }
`;

const CustomLink = styled(Link)`
    color: white;
    margin-left: 5px;
`;

const Register = () => {
    const [firstName, setFName] = useState('');
    const [lastName, setLName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [message, setMessage] = useState('');
    const [severity, setSeverity] = useState<AlertColor>('success');
    const [open, setOpen] = useState(false);
    const role = 'client';
    const { t } = useTranslation();
    const navigate = useNavigate();

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const submitMail = () => {
        if (firstName !== "" && lastName !== "" && email !== "" && password !== "") {
            setFName("");
            setLName("");
            setEmail("");
            setPassword("");

            axios.post('http://127.0.0.1:8000/api/users', { firstName, lastName, email, password, role })
                .then(({ data }) => {
                    if (data === "ERROR") {
                        setSeverity("error");
                        setOpen(true);
                        setMessage('Failed registration!');
                    }
                    else {
                        setSeverity("success");
                        setOpen(true);
                        setMessage('Successful registration!');
                        navigate("/login");
                    }
                })
        }
        else {
            setSeverity("error");
            setOpen(true);
            setMessage('Failed registration!');
        }
    }

    return (
        <CustomMainContainer>
            <h2>{t('REGISTER')}</h2>
            <div className="contactContent">
                <form className="contactForm">
                    <label htmlFor="firstName">{t('FIRST_NAME')}</label>
                    <input onChange={(e) => setFName(() => e.target.value)} type="text" id="firstName" name="firstName" value={firstName} /><br />

                    <label htmlFor="lastName">{t('LAST_NAME')}</label>
                    <input type="text" onChange={(e) => setLName(() => e.target.value)} id="lastName" name="lastName" value={lastName} /><br /><br />

                    <label htmlFor="email">{t('EMAIL')}</label>
                    <input type="text" onChange={(e) => setEmail(() => e.target.value)} id="email" name="email" value={email} /><br /><br />

                    <label htmlFor="password">{t('PASSWORD')}</label>
                    <input type="password" autoComplete="on" onChange={(e) => setPassword(() => e.target.value)} id="password" name="password" value={password} /><br /><br />

                    <div className="buttonContactForm">
                        <button className="contactButton" id="contactButton" onClick={(e) => { e.preventDefault(); submitMail() }}>{t('SUBMIT')}</button>
                    </div>
                </form>
            </div>
            <div className="snackbar">
                <Snackbar open={open} onClose={handleClose} autoHideDuration={1000} >
                    <Alert severity={severity} sx={{ width: '100%' }}>
                        {message}
                    </Alert>
                </Snackbar>
            </div>
            <div className="loginLink"> {t('ALREADY_HAVE_AN_ACCOUNT')}<CustomLink to="/login">Login</CustomLink>. </div>
        </CustomMainContainer>
    );
}

export default Register;