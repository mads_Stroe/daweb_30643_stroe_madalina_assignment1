
import { useTranslation } from 'react-i18next';
import CustomNavbar from './CustomNavbar';
import './index.css';
import axios from "axios";
import { useState } from 'react';

const Contact = () => {
  const { t } = useTranslation();
  const [message, setMessage] = useState('');
  const [name, setName] = useState('');

  const submitMail = () => {   
    axios.post('http://127.0.0.1:8000/send-email', {name, message});
  }

  return (
    <div className="mainContentContact">
      <CustomNavbar />

      <div className="contactDiv">
        <div className="contactContent">
          <form className="contactForm">
            <label htmlFor="fname">{t('FIRST_NAME')}</label>
            <input onChange={(e) => setName(() => e.target.value)} type="text" id="name" name="name" placeholder="John" /><br />
            <label htmlFor="lname">{t('LAST_NAME')}</label>
            <input type="text" id="lname" name="lname" placeholder="Doe" /><br /><br />
            <div className="messageContent"> <label htmlFor="message">{t('WRITE_A_MESSAGE_HERE')}</label><br />
              <textarea  onChange={(e) => setMessage(() => e.target.value)} id="message" rows={5} cols={60} name="message">

              </textarea><br />
            </div>
            <div className="buttonContactForm">
              <button className="contactButton" id="contactButton" onClick={(e) => {e.preventDefault(); submitMail()}}>{t('SUBMIT')}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Contact;