import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import CustomNavbar from './CustomNavbar';
import './index.css';

const DespreNoi = () => {
    const { t } = useTranslation();
    return (
        <div className="mainContentDespreNoi">
            <CustomNavbar />

            <div className="despreNoi">
                <div className="despreNoiContent">
                    <h4>{t('DELUXE_AGENCY')}</h4>
                    <hr />
                    <div>
                        <p>{t('DESC_ABOUT_US_1')} <br /><br /><br /><br />{t('DESC_ABOUT_US_2')}
                        </p>

                        <div className="button">
                            <Link to="/contact"><button className="contactButton">{t('Contact')}</button></Link>
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DespreNoi;