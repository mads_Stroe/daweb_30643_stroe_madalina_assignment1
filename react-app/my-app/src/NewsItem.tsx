import { useTranslation } from "react-i18next";

const NewsItem = (item: {img: string, description: string, alt: string}) => {
    const { t } = useTranslation();

    return (
        <div className="card">
            <img src={`${item.img}`} height={100} width={100} className="card-img" alt={item.alt} />
            <div className="cardContent">
                <p>{item.description}</p>
                <div className="button">
                    <button className="contactButton">{t('SEE_MORE')}</button>
                </div>
            </div>
        </div>
    );
}

export default NewsItem;