import { useTranslation } from 'react-i18next';
import CustomNavbar from './CustomNavbar';
import './index.css';

const Home = () => {
    const { t } = useTranslation();

    return (
        <div className="mainContentHomepage">

            <CustomNavbar />            
            <div className="homepage">
                <div className="homepageContent">
                    <h2 className="discoverTitle">{t('DISCOVER_YOUR_NEW_HOME')}</h2>

                    <form className="searchForm">
                        <input className="searchInput" type="text" name="searchInput" /><br /><br />

                        <button className="contactButton">{t('SEARCH')}</button>
                    </form>

                </div>
            </div>
        </div>
    );
}

export default Home;