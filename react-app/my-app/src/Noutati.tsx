import CustomNavbar from './CustomNavbar';
import './index.css';
import { useEffect, useState } from 'react';
import NewsItem from './NewsItem';

import ap1 from './images/ap1.jpg';
import ap2 from './images/ap2.jpg';
import ap3 from './images/ap3.jpg';
import ap4 from './images/ap4.jpg';
import ap5 from './images/ap5.jpg';
import ap6 from './images/ap6.jpeg';

interface NewsInterface {
    title: string;
    content: string;
}

const Noutati = () => {
    const [news, setNews] = useState<NewsInterface[]>([]);
    const keys: string[] = [];
    const values: string[] = [];
    const newsArray: NewsInterface[] = [];

    const buildMap = (keys: string[], values: string[]) => {
        const map = new Map();
        for (let i = 0; i < keys.length; i++) {
            map.set(keys[i], values[i]);
        };
        return map;
    };

    useEffect(() => {
        const xmlText = '<newsSection>\
                            <news>\
                                <title>Title 1</title>\
                                <content>Content 1</content>\
                            </news>\
                            <news>\
                                <title>Title 2</title>\
                                <content>Content 2</content>\
                            </news>\
                            <news>\
                                <title>Title 3</title>\
                                <content>Content 3</content>\
                            </news>\
                            <news>\
                                <title>Title 4</title>\
                                <content>Content 4</content>\
                            </news>\
                            <news>\
                                <title>Title 5</title>\
                                <content>Content 5</content>\
                            </news>\
                            <news>\
                                <title>Title 6</title>\
                                <content>Content 6</content>\
                            </news>\
                        </newsSection>';

        var XMLParser = require('react-xml-parser');
        var xml = new XMLParser().parseFromString(xmlText);    // Assume xmlText contains the example XML
        const elementByTitle = xml.getElementsByTagName('title');
        const elementByContent = xml.getElementsByTagName('content');

        elementByTitle.forEach((n: any) => {
            keys.push(n.value);
        });

        elementByContent.forEach((n: any) => {
            values.push(n.value);
        });

        const myMap = buildMap(keys, values);
        myMap?.forEach((values, keys) => {
            const aux: NewsInterface = { title: keys, content: values };
            newsArray.push(aux);
        })
        setNews(newsArray);
      }, []);

    return (
        <div>
            <CustomNavbar />

            <div className="noutati">
                <NewsItem img={ap1} description={`${news[0]?.content}`} alt={news[0]?.title} />
                <NewsItem img={ap2} description={`${news[1]?.content}`} alt={news[1]?.title}/>
                <NewsItem img={ap3} description={`${news[2]?.content}`} alt={news[2]?.title}/>
                <NewsItem img={ap4} description={`${news[3]?.content}`} alt={news[3]?.title}/>
                <NewsItem img={ap5}description={`${news[4]?.content}`} alt={news[4]?.title}/>
                <NewsItem img={ap6} description={`${news[5]?.content}`} alt={news[5]?.title}/>
            </div>
        </div>
    );
}

export default Noutati;
