import React, { ReactElement, useContext } from 'react';
import { Navigate, Outlet, PathRouteProps, Route, RouteProps } from 'react-router-dom';
import { UserContext } from './UserContext';

const AuthenticatedRoute = () => {
  const { role } = useContext(UserContext);

  if(!role) return <Navigate to='/login'/>;

  return <Outlet/>;
};


export default AuthenticatedRoute;