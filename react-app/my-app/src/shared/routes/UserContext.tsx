import React, { createContext, useState } from 'react';

interface UserContextModel {
    role: any;
    setRole: (value: string) => void;
}

export const UserContext = createContext<UserContextModel>({ role: 'initial', setRole: () => { } });


export const UserContextProvider = ({ children }: any) => {
    const [role, setRole] = useState(localStorage.getItem('role'));

    const data = {
        role: role,
        setRole: setRole,
    };

    return <UserContext.Provider value={data}>
        {children}
    </UserContext.Provider>;
};