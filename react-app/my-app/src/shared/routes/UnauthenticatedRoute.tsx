import { Component, ReactElement, useContext } from 'react';
import { Navigate, Outlet, Route } from 'react-router-dom';
import { UserContext } from './UserContext';

const UnauthenticatedRoute = () => {
  const { role } = useContext(UserContext);

  if (role) return <Navigate to='/home' />
  return <Outlet/>;

}

export default UnauthenticatedRoute;