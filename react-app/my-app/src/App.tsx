import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Contact from './Contact';
import DespreNoi from './DespreNoi';
import Home from "./Home";
import Noutati from './Noutati';
import './index.css';
import Inchirieri from './Inchirieri';
import Vanzari from './Vanzari';
import Login from './Login';
import Register from './Register';
import UserProfile from './UserProfile';
import LocationPage from './LocationPage';
import ViewReservations from './ViewReservations';
import { UserContextProvider } from './shared/routes/UserContext';
import UnauthenticatedRoute from './shared/routes/UnauthenticatedRoute';
import AuthenticatedRoute from './shared/routes/AuthenticatedRoute';
import { Fragment } from 'react';
import AdaugaLoc from './AdaugaLoc';

const App = () => {

  return (
    <UserContextProvider>
      <Router>
        <Fragment>
          <Routes>

            <Route path='/' element={<UnauthenticatedRoute />}>
              <Route path='/' element={<Register />} />
            </Route>

            <Route path='/login' element={<UnauthenticatedRoute />}>
              <Route path='/login' element={<Login />} />
            </Route>

            <Route path='/home' element={<AuthenticatedRoute />}>
              <Route path='/home' element={<Home />} />
            </Route>

            <Route path='/adauga' element={<AuthenticatedRoute />}>
              <Route path='/adauga' element={<AdaugaLoc />} />
            </Route>

            <Route path='/contact' element={<AuthenticatedRoute />}>
              <Route path='/contact' element={<Contact />} />
            </Route>

            <Route path='/despre' element={<AuthenticatedRoute />}>
              <Route path='/despre' element={<DespreNoi />} />
            </Route>

            <Route path='/noutati' element={<AuthenticatedRoute />}>
              <Route path='/noutati' element={<Noutati />} />
            </Route>

            <Route path='/inchirieri' element={<AuthenticatedRoute />}>
              <Route path='/inchirieri' element={<Inchirieri />} />
            </Route>

            <Route path='/vanzari' element={<AuthenticatedRoute />}>
              <Route path='/vanzari' element={<Vanzari />} />
            </Route>

            <Route path='/profile' element={<AuthenticatedRoute />}>
              <Route path='/profile' element={<UserProfile />} />
            </Route>

            <Route path='/location/:location_id' element={<AuthenticatedRoute />}>
              <Route path='/location/:location_id' element={<LocationPage />} />
            </Route>

            <Route path='/reservation/:location_id' element={<AuthenticatedRoute />}>
              <Route path='/reservation/:location_id' element={<ViewReservations />} />
            </Route>

          </Routes>
        </Fragment>
      </Router>
    </UserContextProvider>
  );
};

export default App;
