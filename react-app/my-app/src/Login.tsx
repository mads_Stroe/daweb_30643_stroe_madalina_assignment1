import axios from "axios";
import { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import styled from 'styled-components';
import Snackbar from '@mui/material/Snackbar';
import Alert, { AlertColor } from "@mui/material/Alert";
import { Link } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import { UserContext } from "./shared/routes/UserContext";

const CustomMainContainer = styled.div`
height: 100%;
width: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
color: white;

.loginLink {
    width: 100%;
    height:3%;
    font-size: 20px;
    display: flex;
    justify-content: center;
}

.login {
    height: 10%;
    margin: 10px;
}
`;

const CustomLink = styled(Link)`
color: white;
margin-left: 5px;
`;

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [message, setMessage] = useState('');
    const [severity, setSeverity] = useState<AlertColor>('success');
    const [open, setOpen] = useState(false);
    const { setRole } = useContext(UserContext);


    const { t } = useTranslation();
    const navigate = useNavigate();

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const submitLogin = () => {
        if (email !== "" && password !== "") {
            setEmail("");
            setPassword("");

            axios.post('http://127.0.0.1:8000/api/login', { email, password })
                .then(({ data }) => {
                    if (data === "not found") {
                        setSeverity("error");
                        setOpen(true);
                        setMessage('Failed login!');
                    }
                    else {
                        localStorage.setItem('firstName', data.firstName);
                        localStorage.setItem('email', data.email);
                        localStorage.setItem('role', data.role);
                        localStorage.setItem('userId', data.id);
                        setRole(data.role);

                        navigate("/home");
                    }
                })
        }
        else {
            setSeverity("error");
            setOpen(true);
            setMessage('Failed login!');
        }
    }

    return (
        <CustomMainContainer>
            <h2>{t('LOGIN')}</h2>
            <div className="contactContent">
                <form className="contactForm">

                    <label htmlFor="email">{t('EMAIL')}</label>
                    <input className="login" type="text" onChange={(e) => setEmail(() => e.target.value)} id="email" name="email" value={email} /><br /><br />

                    <label htmlFor="password">{t('PASSWORD')}</label>
                    <input className="login" type="password" autoComplete="on" onChange={(e) => setPassword(() => e.target.value)} id="password" name="password" value={password} /><br /><br />

                    <div className="buttonContactForm">
                        <button className="contactButton" id="contactButton" onClick={(e) => { e.preventDefault(); submitLogin() }}>{t('SUBMIT')}</button>
                    </div>
                </form>
            </div>
            <div className="snackbar">
                <Snackbar open={open} onClose={handleClose} autoHideDuration={1000} >
                    <Alert severity={severity} sx={{ width: '100%' }}>
                        {message}
                    </Alert>
                </Snackbar>
            </div>
            <div className="loginLink"> {t('DONT_HAVE_AN_ACCOUNT')}<CustomLink to="/">Register</CustomLink>. </div>

        </CustomMainContainer>
    );
}

export default Login;