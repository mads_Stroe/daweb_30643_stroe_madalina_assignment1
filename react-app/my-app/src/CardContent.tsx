import { useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { LocationInterface } from "./Inchirieri";
import small3 from './images/small3.jpg';

const CardContent = (locationItem: LocationInterface) => {
    const { t } = useTranslation();
    const isClient = localStorage.getItem('role') === "client" ? true : false;
    const userId = localStorage.getItem('userId') !== "" ? localStorage.getItem('userId') : 0;
    const navigate = useNavigate();
    const isAdmin = localStorage.getItem('role') === "AI" ? true : false;

    const submitOrder = (id: any, description: string, address: string, userId: string) => {
        console.log(id + ' ' + description + ' ' + address + ' ' + userId);
        navigate(`/location/${id}`);
    }

    const viewDetailsAboutLocation = (location_id: any) => {
        navigate(`/reservation/${location_id}`);
    }

    return (
        <div className="card" id={locationItem.id}>
            <div className="title">{locationItem.description}</div>
            <img src={small3} height={100} width={100} className="card-img" alt="ap1" />
            <div className="cardContent">
                <div className="button">
                    <label className="locationName">{locationItem.name}</label>
                    { isClient && !isAdmin && <button onClick={() => submitOrder(locationItem.id, locationItem.description, locationItem.address, userId as string)} className="contactButton">{t('SELECT')}</button> }
                    { isAdmin && !isClient && <button onClick={()=>viewDetailsAboutLocation(locationItem.id)} className="contactButton">{t('VIEW')}</button> }

                </div>
            </div>
        </div>
    );
}

export default CardContent;