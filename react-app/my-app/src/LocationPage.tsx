import { Alert, AlertColor, Snackbar, TextField } from "@mui/material";
import axios from "axios";
import moment from "moment";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate, useParams } from "react-router-dom";
import styled from 'styled-components';
import CustomNavbar from "./CustomNavbar";

interface UserLocationInterface {
    user_id: number;
    location_id: number;
    start: string;
    end: string;
}

const CustomTextField = styled(TextField)`

    &.MuiFormControl-root {
        .MuiInputLabel-root {
            color: white;
        }

        .MuiOutlinedInput-root {
            color: white;
        }

    }
`;

export interface Reservation {
    title: string;
    start: Date;
    end: Date;
}

const LocationPage = () => {
    const { location_id } = useParams();
    const { t } = useTranslation();
    const navigate = useNavigate();
    const [date, setDate] = useState('2022-04-04T18:30');
    const [message, setMessage] = useState('');
    const [severity, setSeverity] = useState<AlertColor>('success');
    const [open, setOpen] = useState(false);
    const userId = localStorage.getItem('userId');
    const [reservations, setReservations] = useState<any[]>([]);
    const reservationsArray: any[]  = [];


    const [loc, setLoc] = useState({
        address: "",
        created_at: "",
        description: "",
        id: 0,
        image: "",
        updated_at: ""
    });


    const submitReservation = (user_id: string, date: string) => {
        const timeArray = date.split("T")[1].split(":");
        const dataAux = date.split("T")[0];
        let hour = parseInt(timeArray[0]);
        let minutes = parseInt(timeArray[1]);
        const start = String(hour) + ":" + String(minutes);
        let end: String = '';

        if(minutes == 30) {
            hour = hour + 1;
            minutes += 30;
            if(minutes == 60) {
                minutes = 0;
            }
        }
        else {
            minutes += 30;
        }

        if(minutes == 0) {
           end = String(minutes)+'0';
        }
        else {
            end = String(minutes);
        }

        end = dataAux+"T"+hour + ":"+end;

        const result: UserLocationInterface = {
            user_id: Number(user_id),
            location_id: Number(location_id),
            start: date, 
            end: String(end)
        };
        
        axios.post('http://127.0.0.1:8000/api/usersLocations', result)
            .then(({ data }) => {
                if (data == "ERROR") {
                    setSeverity("error");
                    setOpen(true);
                    setMessage('Pick another date or time.');
                }
                else {
                    setSeverity("success");
                    setOpen(true);
                    setMessage('Successful reservation!');

                    fetchData();
                    // navigate("/home");
                }
            })
    };

    useEffect(() => {
        axios.get(`http://127.0.0.1:8000/api/locations/${location_id}`)
            .then(({ data }) => {
                setLoc(data);
            })
    }, []);

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    //reservations

    const fetchData = async () => {
        await axios.post('http://127.0.0.1:8000/api/usersLocations/getReservations', { location_id })
        .then(({ data }) => {
            setReservations(data);
            data.forEach((res: any) => {
                const reservation: Reservation = {
                    title: res.user,
                    start: res.date.start.replace("T", " "),
                    end: res.date.end.replace("T", " ")
                }
                reservationsArray.push(reservation);
            })
        });
        setReservations(reservationsArray);
        console.log(reservationsArray);
        localStorage.setItem('reservations', JSON.stringify(reservationsArray));
    };

    return (
        <div className="mainContentContact">

            <CustomNavbar />
            <div className="contactDiv">
                {/* <h4> Schedule your visit </h4> */}
                <div className="contactContent">


                    <h3>Type: {loc?.description}</h3>
                    <h3>Address: {loc?.address}</h3>

                    <CustomTextField
                        id="datetime-local"
                        label="Make appointment"
                        type="datetime-local"
                        sx={{ width: 250 }}
                        InputLabelProps={{
                            shrink: true,
                        }}
                        value={date}
                        onChange={(e) => setDate(e.target.value)}
                    />

                    <div className="buttonContactForm">
                        <button className="contactButton" onClick={(e) => { e.preventDefault(); submitReservation(userId!, date); }} id="contactButton">{t('SUBMIT')}</button>
                        <button className="contactButton" onClick={(e) => { e.preventDefault(); navigate('/home'); }} id="contactButton">{t('CANCEL')}</button>

                    </div>
                </div>

                <div className="snackbar">
                    <Snackbar open={open} onClose={handleClose} autoHideDuration={1000} >
                        <Alert severity={severity} sx={{ width: '100%' }}>
                            {message}
                        </Alert>
                    </Snackbar>
                </div>

            </div>
        </div>
    );
}

export default LocationPage;