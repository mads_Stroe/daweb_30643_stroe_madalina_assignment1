import { Card } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import CardContent from "./CardContent";
import CustomNavbar from "./CustomNavbar";

export interface LocationInterface {
    id: string;
    description: string;
    name: string;
    address: string;
}

const Inchirieri = () => {
    const { t } = useTranslation();
    const locations: LocationInterface[] = [];
    const studio: LocationInterface[] = [];
    const apartment: LocationInterface[] = [];
    const house: LocationInterface[] = [];
    const [aux, setAux] = useState<LocationInterface[]>([]);
    const [studios, setStudios] = useState<LocationInterface[]>([]);
    const [apartments, setApartments] = useState<LocationInterface[]>([]);
    const [houses, setHouses] = useState<LocationInterface[]>([]);

    const fetchData = () => {
        axios.get('http://127.0.0.1:8000/api/locations')
            .then(({ data }) => {
                data.forEach((x: LocationInterface) => {
                    if (x.description == 'Studio') {
                        studio.push(x);
                    }
                    else if (x.description == 'Apartment') {
                        apartment.push(x);
                    }
                    else if (x.description == 'House') {
                        house.push(x);
                    }

                    locations.push(x);

                });
                setStudios(studio);
                setApartments(apartment);
                setHouses(house);
                setAux(locations);
            })
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <CustomNavbar />

            <div className="noutati">
                <div id="garsoniereSection" className="noutati">
                    {studios.map((x: LocationInterface) => <CardContent key= {x?.id} id={x?.id} description={x?.description} name={x?.name} address={x?.address} />)}
                </div>

                <div id="apartamentSection" className="noutati">
                    {apartments.map((x: LocationInterface) => <CardContent key= {x?.id} id={x?.id} description={x?.description} name={x?.name} address={x?.address} />)}
                </div>

                <div id="homeSection" className="noutati">
                    {houses.map((x: LocationInterface) => <CardContent key= {x?.id} id={x?.id} description={x?.description} name={x?.name} address={x?.address} />)}
                </div>

            </div></div>
    );
};

export default Inchirieri;