import { Divider, List, ListItem, Switch } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import LocationReservation from "./LocationReservation";
import styled from 'styled-components';
import { Scheduler } from "@aldabil/react-scheduler";

import {
    LineChart,
    ResponsiveContainer,
    Legend, Tooltip,
    Line,
    XAxis,
    YAxis,
    CartesianGrid
} from 'recharts';

export interface ReservationInterface {
    user: string;
    date: string;
}

const CustomListItem = styled(ListItem)`
    width: 100%;
    margin: 0 auto;
    height: 100%
    border: 2px solid red;
    background: rgb(46, 58, 47);
    color: white;
`;

const CustomDivider = styled(Divider)`
    width: 100%;
`;

const CustomResponsiveContainer = styled(ResponsiveContainer)`
    border: 2px solid green;
`;

const CustomDiv = styled.div`
    height: 100%;
    background: rgb(75, 78, 75);
`;

interface numberOfReservations {
    date: string;
    views: any;
}

const label = { inputProps: { 'aria-label': 'Switch demo' } };

const ViewReservations = () => {
    const { location_id } = useParams();
    const [reservations, setReservations] = useState<any[]>([]);
    const auxReservations: any[] = [];
    const [res, setRes] = useState<numberOfReservations[]>([]);
    const counts: any = {};
    const result: numberOfReservations[] = [];
    const [isCalendar, setIsCalendar] = useState(true);

    const handleChange = () => {
        setIsCalendar(!isCalendar);
        if(!isCalendar) {
            getReservations();
        }
        else {
            getNumberOfReservationsPerDay();
        }
    };

    const getReservations = () => {
        axios.post('http://127.0.0.1:8000/api/usersLocations/getReservations', { location_id })
            .then(({ data }) => {
                data.forEach((x: any) => {
                    const res = {

                        title: x.user,
                        start: new Date(x.date.start),
                        end: new Date(x.date.end),
                    }
                    auxReservations.push(res);
                });
                setReservations(auxReservations);
            });
    }

    const getNumberOfReservationsPerDay = () => {
        axios.post('http://127.0.0.1:8000/api/usersLocations/getReservations', { location_id })
            .then(({ data }) => {
                setReservations(data);

                for (const num of data) {
                    const currentDate = num.date.start.split(" ")[0];
                    counts[currentDate] = counts[currentDate] ? counts[currentDate] + 1 : 1;
                }

                let auxObj = JSON.parse(JSON.stringify(counts));

                Object.entries(auxObj).forEach(([key, value]) => {
                    const aux: numberOfReservations = {
                        date: key,
                        views: value
                    };
                    result.push(aux);
                });
                setRes(result);
            })
    }

    useEffect(() => {
        getReservations();
    }, [isCalendar]);

    return (
        <div className="reservation">
            <div className="switchDiv">
                Graph
                <Switch checked={isCalendar} onClick={handleChange} />
                Calendar

            </div>

            {(isCalendar === true) && <CustomDiv>
                <Scheduler
                    view="month"
                    events={reservations}
                />
            </CustomDiv>}

            {(isCalendar === false) && <div className="chartDiv">
                <ResponsiveContainer width="100%" aspect={3}>
                    <LineChart data={res} margin={{ right: 300 }}>
                        <CartesianGrid />
                        <XAxis dataKey="date"
                            interval={'preserveStartEnd'} />
                        <YAxis></YAxis>
                        <Legend />
                        <Tooltip />
                        <Line dataKey="views"
                            stroke="black" activeDot={{ r: 8 }} />
                    </LineChart>
                </ResponsiveContainer>
            </div>}
        </div>
    );
}

export default ViewReservations;