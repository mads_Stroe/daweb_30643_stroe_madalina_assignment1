import CustomNavbar from "./CustomNavbar";
import { ReservationInterface } from "./ViewReservations";


const LocationReservation = (item: {user: string, date: string}) => {
    return (
        <div className="reservationItem">
            <div className="user">
                User: <br/>
                {item.user}
            </div>
            <div className="user">
                Date: <br/>
                {item.date}
            </div>
        </div>
    );
}

export default LocationReservation;