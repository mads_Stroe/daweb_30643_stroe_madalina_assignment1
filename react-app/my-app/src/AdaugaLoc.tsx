import axios from "axios";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from 'styled-components';
import Snackbar from '@mui/material/Snackbar';
import Alert, { AlertColor } from "@mui/material/Alert";
import { Link, useNavigate } from "react-router-dom";
import CustomNavbar from "./CustomNavbar";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

const CustomMainContainer = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    color: white;

    .loginLink {
        width: 100%;
        height:3%;
        font-size: 20px;
        display: flex;
        justify-content: center;
    }
`;

const CustomFormControl = styled(FormControl)`
    &.MuiFormControl-root {
        margin: 20px;

        .MuiInputLabel-root {
            color: white;
        }
    }
`;


const AdaugaLoc = () => {
    const { t, i18n } = useTranslation()
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [address, setAddress] = useState('');
    const [message, setMessage] = useState('');
    const [severity, setSeverity] = useState<AlertColor>('success');
    const [open, setOpen] = useState(false);

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const addNewLocation = (location: { name: string, description: string, address: string }) => {
        console.log(location);
        if (name == "" || description == "" || address == "") {
            setSeverity("error");
            setOpen(true);
            setMessage('All fields required.');
        }
        else {
            axios.post('http://127.0.0.1:8000/api/locations', location)
                .then(({ data }) => {
                    if (data === "ERROR") {
                        setSeverity("error");
                        setOpen(true);
                        setMessage('Could not add new location.');
                    }
                    else {
                        setSeverity("success");
                        setOpen(true);
                        setMessage('New location added.');
                    }
                })
        }

        setName("");
        setDescription("");
        setAddress("");
    }

    return (
        <div className="mainContentDespreNoi">
            <CustomNavbar />
            <CustomMainContainer>
                <h2>{t('ADD_NEW_LOCATION')}</h2>
                <div className="contactContent">
                    <form className="contactForm">
                        <label htmlFor="name">{t('NAME')}</label>
                        <input onChange={(e) => setName(() => e.target.value)} type="text" id="name" name="name" value={name} /><br />

                        <CustomFormControl >
                            <InputLabel className="formDescription" id="demo-simple-select-label">{t('DESCRIPTION')}</InputLabel>
                            <Select
                                value={description}
                                label="Description"
                                onChange={(e: any) => setDescription(e.target.value)}
                            >
                                <MenuItem value={'Studio'}>{t('STUDIO')}</MenuItem>
                                <MenuItem value={'Apartment'}>{t('APARTMENT')}</MenuItem>
                                <MenuItem value={'House'}>{t('HOUSE')}</MenuItem>
                            </Select>
                        </CustomFormControl>

                        <label htmlFor="address">{t('ADDRESS')}</label>
                        <input type="text" onChange={(e) => setAddress(() => e.target.value)} id="address" name="address" value={address} /><br /><br />

                        <div className="buttonContactForm">
                            <button className="contactButton" id="contactButton" onClick={(e) => { e.preventDefault(); addNewLocation({ name, description, address }); }}>{t('ADD')}</button>
                        </div>
                    </form>
                </div>
                <div className="snackbar">
                    <Snackbar open={open} onClose={handleClose} autoHideDuration={1000} >
                        <Alert severity={severity} sx={{ width: '100%' }}>
                            {message}
                        </Alert>
                    </Snackbar>
                </div>
            </CustomMainContainer>
        </div>
    );
};

export default AdaugaLoc;