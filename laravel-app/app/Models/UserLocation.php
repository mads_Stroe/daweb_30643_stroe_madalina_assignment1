<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model
{
    protected $table = 'usersLocations';
    
    protected $fillable = [
        'user_id',
        'location_id',
        'start', 
        'end'
    ];}
