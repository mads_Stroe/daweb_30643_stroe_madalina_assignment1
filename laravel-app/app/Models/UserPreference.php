<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPreference extends Model
{
    protected $table = 'userPreferences';
    
    protected $fillable = [
        'user_id',
        'preference_id'
    ];
}
