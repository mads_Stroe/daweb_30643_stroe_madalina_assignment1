<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Preference extends Model
{
    protected $fillable = [
        'description'
    ];

    //TO DO: rename to users
    public function preferences()
    {
        return $this->belongsToMany(
            User::class,
            'userPreferences'
        )->withTimestamps();
    }
}
