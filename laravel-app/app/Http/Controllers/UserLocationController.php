<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserLocation;
use App\Models\Reservation;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class UserLocationController extends Controller
{
    public function index()
    {
        return UserLocation::all();
    }

    public function show($id)
    {
        $user = UserLocation::where('user_id', $id)->get();

        return $user[0];
    }

    public function store(Request $request)
    {
        try {
            $userLocation = UserLocation::create($request->all());
            return $userLocation;
        } catch (\Illuminate\Database\QueryException $e) {
            return  "ERROR";
        }
    }

    public function date_sort($a, $b)
    {
        return strtotime($a) - strtotime($b);
    }

    // public function getClosest(Request $request)
    // {
    //     try {
    //         $date = $request->input('date');
    //         $nextDate = "";
    //         $allDates = DB::table('usersLocations')->pluck('date')->toArray();
    //         $allTimes = [];
    //         $currentDate = "";
    //         $i = 0;
    //         $isInArray = False;
    //         $nextTime = "";


    //         foreach ($allDates as $x) {
    //             $time = Carbon::parse($x);
    //             $currentDate = date('m-d', strtotime($time));

    //             if (date('m-d', strtotime($date)) == $currentDate) {
    //                 $allTimes[$i] = date('H:i', strtotime($time));
    //                 $i++;
    //             }
    //         }

    //         usort($allDates, function ($x, $y) {
    //             return strtotime($x) - strtotime($y);
    //         });

    //         foreach ($allDates as $count => $dateSingle) {
    //             if (strtotime($date) == strtotime($dateSingle)) {
    //                 $nextDate = Carbon::parse($date);
    //                 $nextTime = date('H:i', strtotime($nextDate));

    //                 if (in_array($nextTime, $allTimes)) {
    //                     $nextDate = $nextDate->addMinutes(30);
    //                     break;
    //                 }
    //             }
    //         }
    //         $nextDate = date('m-d H:i', strtotime($nextDate));
    //         return $nextDate;
    //     } catch (\Illuminate\Database\QueryException $e) {
    //         return  "ERROR";
    //     }
    // }

    public function getReservations(Request $request)
    {
        $locationsArray = UserLocation::where('location_id', $request->location_id)->get(['user_id', 'start', 'end'])->toArray();
        $names[] = [];
        $i = 0;
        $array = [];

        foreach ($locationsArray as $x) {
            $names[$i] = UserController::findNameById($x['user_id']);
            $i++;
        }

        $dates = UserLocation::where('location_id', $request->location_id)->get(['start', 'end'])->toArray();

        $i = 0;
        foreach ($dates as $x) {
            $reservation = new Reservation();
            $reservation->user = $names[$i];
            $reservation->date = $dates[$i];
            array_push($array, $reservation);
            $i++;
        }

        return $array;
    }
}
