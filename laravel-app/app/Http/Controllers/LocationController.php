<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Location;

class LocationController extends Controller
{
    public function index()
    {
        return Location::all();
    }

    public function show($id)
    {
        return Location::find($id);
    }

    public function store(Request $request)
    {
        try {
            $user = Location::create($request->all());
            return $user;
        } catch (\Illuminate\Database\QueryException $e) {
            return  "ERROR";
        }
    }

    public function update(Request $request, $id)
    {
        $user = Location::findOrFail($id);
        $user->update($request->all());

        return $user;
    }

    public function delete(Request $request, $id)
    {
        $user = Location::findOrFail($id);
        $user->delete();

        return 204;
    }
}
