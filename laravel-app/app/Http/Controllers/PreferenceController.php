<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Preference;

class PreferenceController extends Controller
{
    public function index()
    {
        return Preference::all();
    }
 
    public function show($id)
    {
        return Preference::find($id);
    }

    public function store(Request $request)
    {
        try {
            $user = Preference::create($request->all());
            return $user;
          
          } catch (\Illuminate\Database\QueryException $e) {
              return  "ERROR";
          }
    }

    public function update(Request $request, $id)
    {
        $user = Preference::findOrFail($id);
        $user->update($request->all());

        return $user;
    }

    public function delete(Request $request, $id)
    {
        $user = Preference::findOrFail($id);
        $user->delete();

        return 204;
    }

}
