<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Preference;
use App\Models\User;
use App\Models\UserPreference;

class UserPreferenceController extends Controller
{
    public function index()
    {
        return UserPreference::all();
    }

    public function show($id)
    {
        $preferences =  User::find($id)->preferences;
        foreach ($preferences as $p){
            $preferencesArray[] = $p->id;
        }

        return $preferences;
    }

    public function store(Request $request)
    {
        try {
            $user = User::findOrFail($request->input("user_id"));
            $user->preferences()->attach($request->input("preference_id"));

            return $user;
            
        } catch (\Illuminate\Database\QueryException $e) {
            return  "ERROR";
        }
    }

    public function delete(Request $request)
    {
        try {

            $user = User::find($request->input("user_id"));

            $user->preferences()->detach($request['preference_id']);

            return 204;

        } catch (\Illuminate\Database\QueryException $e) {
            return  "ERROR";
        }
    }
}
