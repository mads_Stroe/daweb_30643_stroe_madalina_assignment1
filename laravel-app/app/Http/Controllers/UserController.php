<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function show($id)
    {
        return User::find($id)->preferences->locations();
    }

    public function showByEmail(Request $request)
    {
        $user = User::where('email', $request['email'])->get();

        return $user;
    }

    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $password = encrypt($input['password']);
            $input['password'] = $password;

            $user = User::create($input);

            return $user;
        } catch (\Illuminate\Database\QueryException $e) {
            return  "ERROR";
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $user = User::findOrFail($id);
            $user->update($request->all());

            return $user;
        } catch (\Illuminate\Database\QueryException $e) {
            return  "ERROR";
        }
    }

    public function delete(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return 204;
    }

    // public function login(Request $request)
    // {
    //     $user = User::where('email', $request['email'])
    //         ->where('password', $request['password'])
    //         ->get();

    //     return isset($user[0]) ? $user[0] : "not found";
    // }

    
    public function login(Request $request)
    {
        $user = User::where('email', $request['email'])
            ->get()->first();
        
        $encryptedPassword = $user['password'];

        $decryptedPassword = decrypt($encryptedPassword);

        if($request['password'] == $decryptedPassword) return $user;


        return $decryptedPassword;
    }

    public static function findNameById($id)
    {
        return User::find($id)->firstName;
    }
}
