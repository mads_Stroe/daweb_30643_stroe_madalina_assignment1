<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('users', 'App\Http\Controllers\UserController@index');
Route::get('users/{id}', 'App\Http\Controllers\UserController@show');
Route::post('users', 'App\Http\Controllers\UserController@store');
Route::put('users/{id}', 'App\Http\Controllers\UserController@update');
Route::delete('users/{id}', 'App\Http\Controllers\UserController@delete');
Route::post('login', 'App\Http\Controllers\UserController@login');
Route::post('profile', 'App\Http\Controllers\UserController@showByEmail');
Route::post('users/findNameById', 'App\Http\Controllers\UserController@findNameById');

Route::get('preferences', 'App\Http\Controllers\PreferenceController@index');
Route::get('preferences/{id}', 'App\Http\Controllers\PreferenceController@show');
Route::post('preferences', 'App\Http\Controllers\PreferenceController@store');
Route::put('preferences/{id}', 'App\Http\Controllers\PreferenceController@update');
Route::delete('preferences/{id}', 'App\Http\Controllers\PreferenceController@delete');

Route::get('userPreferences', 'App\Http\Controllers\UserPreferenceController@index');
Route::get('userPreferences/{id}', 'App\Http\Controllers\UserPreferenceController@show'); //ids of user preferences
Route::post('userPreferences', 'App\Http\Controllers\UserPreferenceController@store');
Route::delete('userPreferences/delete', 'App\Http\Controllers\UserPreferenceController@delete');

Route::get('locations', 'App\Http\Controllers\LocationController@index');
Route::get('locations/{id}', 'App\Http\Controllers\LocationController@show');
Route::post('locations', 'App\Http\Controllers\LocationController@store');
Route::put('locations/{id}', 'App\Http\Controllers\LocationController@update');
Route::delete('locations/{id}', 'App\Http\Controllers\LocationController@delete');

Route::get('usersLocations', 'App\Http\Controllers\UserLocationController@index');
Route::get('usersLocations/{id}', 'App\Http\Controllers\UserLocationController@show'); //ids of user preferences
Route::post('usersLocations', 'App\Http\Controllers\UserLocationController@store');
Route::delete('usersLocations/delete', 'App\Http\Controllers\UserLocationController@delete');
Route::post('usersLocations/closest', 'App\Http\Controllers\UserLocationController@getClosest');
Route::post('usersLocations/getReservations', 'App\Http\Controllers\UserLocationController@getReservations');