import json

from django.shortcuts import render
from django.core.mail import send_mail
from django.conf import settings
from rest_framework.response import Response
from rest_framework.decorators import api_view

@api_view(['POST'])
def index(request):
    message = request.body
    aux = message.decode('utf-8')

    bodyResponse = json.loads(aux)
    print(bodyResponse)

    send_mail(
        bodyResponse['name'],
        bodyResponse['message'],
        settings.EMAIL_HOST_USER,
        ["madalinastroe35@gmail.com"],
        fail_silently=False)

    return Response("OK")